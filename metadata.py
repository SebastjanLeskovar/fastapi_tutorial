app = FastAPI(
    title="My Super Project",
    description="This is a very fancy project, with auto docs for the API and everything",
    version="2.5.0",
    openapi_url="/api/v1/openapi.json",
    docs_url="/documentation", redoc_url=None,
)

app.mount("/static", StaticFiles(directory="static"), name="static")


@app.get("/items/")
async def read_items():
    return [{"name": "Foo"}]
